var Todos = require('../models/todoModel');

module.exports = function(app) {
  app.get('/api/setupTodos', function(req, res) {
    //Seed the db here
    var starterItems = [
      {
        username: 'seed',
        todo: 'Seed todo',
        isDone: false,
        hasAttachment: false
      },
      {
        username: 'seed two',
        todo: 'Feed dog',
        isDone: false,
        hasAttachment: false
      },
      {
        username: 'seed three',
        todo: 'Pat dog',
        isDone: false,
        hasAttachment: false
      }
    ];
    Todos.create(starterItems, function(err, results) {
      if(err) throw err;
      if(results) {
        res.send(results);
      }
    });
  });
}
